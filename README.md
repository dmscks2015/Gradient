# 안 읽어도 되는 파일
- `.gitignore`
- `poetry.lock`
- `pyproject.toml`
- `src/core/__init__.py`
- `src/core/Chainable.py`

# src
소스코드가 모여있음

## preference.py
GD(경사하강법)에 필요한 정보들, 함수라던가 반복횟수라던가 저장

## utils.py
그래프 출력하는 코드같은 것 한 파일에 몰아넣으니까 안 이뻐서 넣음

## basic.py
기본적 경사하강법. run 함수로 실행

## accl.py
모멘텀 GD. run 함수로 실행

# src/core
GD를 실제로 여기서 수행함(이쁘라고 분리해둠)

## GradientDescent.py
그저 GradientDescent 클래스가 있을 뿐

### invoke()
주어진 값으로 연산.

### mathscale(x ,dotscale = 3)
x를 `3.822 × 10⁻¹⁰`처럼 이쁘게 출력해줌. 소수점 dotscale짜리 번째까지 출력.

### initial_param
r의 초기값. 여기서부터 r을 찾기 시작

### learning_rate
학습률. 적당하면 빠르게 학습되지만 아니라면 나락

### num_iterations
학습횟수. 0이면 무한정.

### func_differential
x의 최소를 찾을 함수의 *도함수*.

### early_stop_policy
무시해도 됨. 학습 더 이상 의미없을 때 끊을 수 있게 해 주는 용도.

### x
지워야 하는데 코드 수정하기 귀찮아서 남겨둠

### apply()
무시해도 됨. 실제로는 Chainable의 함수임. `GradientDescent().apply(변수1=값1,변수2=값2).invoke()` 식으로 호출 이쁘게 하려고 만듬.