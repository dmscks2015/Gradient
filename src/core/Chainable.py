from typing import Callable, Generic

T = Generic


class Chainable:
    def apply(
        self,
        initial_param: float | None = None,
        learning_rate: Callable[[int], float] | float | None = None,
        num_iterations: int | None = None,
        func_differential: Callable[[float], float] | None = None,
        early_stop_policy: Callable[[int], bool] | None = None,
    ):
        # Check if the parameter is provided before assigning
        if initial_param is not None:
            self.initial_param = initial_param
        if learning_rate is not None:
            self.learning_rate = learning_rate
        if num_iterations is not None:
            self.num_iterations = num_iterations
        if func_differential is not None:
            self.func_differential = func_differential
        if early_stop_policy is not None:
            self.early_stop_policy = early_stop_policy

        return self
