from typing import Callable

import sympy
from core.Chainable import Chainable


class GradientDescent(Chainable):
    def __init__(self, **kwargs) -> None:
        self.initial_param: float
        """initial_param (float): The initial parameter value for the gradient descent."""

        self.learning_rate: Callable[[int, float], float] | float
        """
        learning_rate ((int) -> float | float): The learning rate for the gradient descent.
        If learning_rate is float, rate will be constant.
        If learning_rate is (int)->float, rate will be dynamically changed.
            It's parameter is (iteration, x).
        """

        self.num_iterations: int
        """
        num_iterations (int): How much gradient_descent will be run, and parameter will be tuned.
        if this is 0, there is no way to stop invoke() without error (except early_stop_polity)
        """

        self.func_differential: Callable[[float], float]
        """
        func_differential ((float) -> float) : differential of function which will be targeted.
        Actually, this doesn't have to be differential. It is just a movement-distance-function.
        positive value means move backward, and vise versa.
        """

        self.early_stop_policy: Callable[[int, float], bool] = (
            lambda i, x: self.default_early_stop_policy(i, x)
        )
        """
        early_stop_policy ((int,float) -> bool): When this is True, gradient_descent will be stopped regardness of iter.
        To customize, override this. Default action is when learning rate is 0. Param is (iter,x).
        """

        self.x: float
        """
        x (float): Just an in-tuning param. Be careful not to access this before calling invoke().
        """

        for key, value in kwargs.items():
            self.__dict__[key] = value

    def default_early_stop_policy(self, iteration: int, x: float) -> bool:
        if callable(self.learning_rate):
            return self.learning_rate(iteration, x) == 0.0
        else:
            return False

    @staticmethod
    def mathscale(
        x: float | sympy.core.numbers.Float,
        dotscale: int = 3,
        splitter: str = " × ",
    ) -> str:
        upperstr = {
            i[0]: i[1].replace("_", "")
            for i in "0⁰,1¹,2²,3³,4⁴,5⁵,6⁶,7⁷,8⁸,9⁹,-⁻,+_".split(",")
        }
        fmt = str("%." + str(dotscale) + "e")
        floatscale, lgscale = (fmt % x).split("e")
        lgscale = "".join([upperstr[i].replace("_", "") for i in list(lgscale)])
        return f"{floatscale}{splitter}10{lgscale}"

    def invoke(
        self,
    ) -> float:
        """
        Performs gradient descent optimization on a given function.
        Returns(float): The optimized parameter value after performing gradient descent. Try put this value to original function or Integral(func_differential).
        """
        self.x = self.initial_param
        learning_rate: Callable[[int, float], float] = (
            self.learning_rate
            if callable(self.learning_rate)
            else eval(f"lambda x,y:{self.learning_rate}")
        )
        try:
            import itertools

            for iteration in (
                range(self.num_iterations)
                if self.num_iterations != 0
                else itertools.count()
            ):
                if self.early_stop_policy(iteration, self.x):  # early-stop
                    print("Gradient descent finished learning, early-stopped.")
                    break

                gradient = self.func_differential(self.x)

                update = learning_rate(iteration, self.x) * gradient
                self.x = self.x - update
                if iteration % 10 == 0:
                    print(
                        f"{iteration} -> x = {self.mathscale(self.x)}, df/dx = {self.mathscale(gradient)}"
                    )
        except KeyboardInterrupt:
            print("Gradient descent halted due to user inturrupt")
        finally:
            try:
                print(
                    f"{iteration} -> x = {self.mathscale(self.x)}, df/dx = {self.mathscale(gradient)}"
                )
            except:
                pass
        return self.x
