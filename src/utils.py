import sympy

method_name = 'print("Method:", __file__.split("/")[-1].strip(".py"))'


def pause() -> None:
    return
    __import__("os").system(
        "/bin/bash -c 'read -s -n 1 -p \"Press any key to continue...\n\"'"
        if __import__("platform").system() == "Linux"
        else "pause"
    )


def plot(
    func,
    param: sympy.core.symbol.Symbol,
    x_lim: tuple[float, float],
    y_lim: tuple[float, float],
    blocking: bool = True,
) -> None:
    def internal_plt():
        import spb

        try:
            spb.graphics(
                spb.line(func, (param, x_lim[0], x_lim[1]), adaptive=True),
                ylim=y_lim,
                grid=True,
            ).save("/tmp/asdf.png")
        except KeyboardInterrupt:
            pass

    if blocking:
        internal_plt()
    else:
        import multiprocessing as mp

        mp.Process(name="Plotter", target=internal_plt).start()


def stat(result: float):
    import preference
    from core import GradientDescent

    print("Optimal parameter:", GradientDescent.mathscale(result, dotscale=10))
    print(
        "Error(%):",
        GradientDescent.mathscale(preference.answer * 100 / result - 100, dotscale=10),
    )

    pause()


def template_early_stop(it: int, x: float) -> bool:
    from preference import func_difference, r
    import os

    ERROR = 10 ** int(os.environ.get("ERROR","-15"))
    return ERROR > func_difference.subs(r, x) and func_difference.subs(r, x) > -ERROR
