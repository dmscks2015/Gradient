import sympy
from sympy.core.expr import Expr as __Expr__
import os
ANSTRONG: float = 10**-10  # Å (단위, 0.1nm)
ZEPTO: float = 10**-21  # z (SI 접두사)

EPSILON = 1.654 * ZEPTO  # J
RO = 3.405 * ANSTRONG
r: sympy.Symbol
func: __Expr__
func_difference: __Expr__
answer: float

INIT_PARAM = RO
NUM_ITER =int(os.environ.get("STEPS","3000"))

XLIM = (0.2 * ANSTRONG, 10 * ANSTRONG)
YLIM = (-3 * ZEPTO, 5 * ZEPTO)


def pickeled(
    finddir: str | None = None, findpath: str | None = None
) -> tuple[sympy.Symbol, __Expr__, __Expr__, float]:

    def load_from_pickle() -> tuple[sympy.Symbol, __Expr__, __Expr__, float]:
        assert (
            finddir is not None and findpath is not None
        ), "Both finddif and findpath is None."
        import os

        path: str
        if findpath is not None:
            assert os.path.isfile(findpath), "There is no findpath file."
            path = findpath
        if finddir is not None:
            assert os.path.isdir(finddir), "There is no finddir dir."
            assert os.path.isfile(
                finddir + "/ckpt.pickle"
            ), "There is no ckpt.pickle file in finddir."
            path = finddir + "/ckpt.pickle"

        import pickle

        with open(path, "rb") as fr:
            return pickle.load(fr)

    try:
        return load_from_pickle()
    except AssertionError:
        r = sympy.Symbol("r", real=True)
        funct = 4 * EPSILON * ((RO / r) ** 12 - (RO / r) ** 6)
        funcdiff = funct.diff(r)
        return (
            r,
            funct,
            funct.diff(r),
            next(
                filter(
                    lambda x: x[r].is_real and x[r] > 0.0,
                    sympy.solve(
                        funcdiff,
                        r,
                        dict=True,
                        check=False,
                    ),
                )
            )[r],
        )


r, func, func_difference, answer = pickeled()
