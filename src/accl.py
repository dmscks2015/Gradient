from core import GradientDescent
import sympy
import numpy as np
from sympy.core.expr import Expr


def run():
    import sys
    import utils
    import preference
    from preference import r

    if sys.argv[-1] == "--plot":
        utils.plot(
            preference.func,
            preference.r,
            blocking=False,
            x_lim=preference.XLIM,
            y_lim=preference.YLIM,
        )

    def accl_m2():

        # Control with learning rate
        MAX_LRRATE = 0.5
        MIN_LRRATE = 0.005
        dodofunction: Expr = preference.func_difference.diff(r)

        def difference(t: float) -> float:
            dif = preference.func_difference.subs(r, t)
            return dif

        def lrrate(t: int, x: float) -> float:
            return min(MIN_LRRATE, max(abs(dodofunction.subs(r, x)), MAX_LRRATE))

        return difference, lrrate

    method = accl_m2()
    result: float = (
        GradientDescent()
        .apply(
            initial_param=preference.INIT_PARAM,
            num_iterations=preference.NUM_ITER,
            func_differential=method[0],
            learning_rate=method[1],
            early_stop_policy=utils.template_early_stop,
        )
        .invoke()
    )
    exec(utils.method_name)
    utils.stat(result)


if __name__ == "__main__":
    run()
