def run():
    import preference
    from core import GradientDescent
    import utils
    import sys
    import sympy
    import os
    if sys.argv[-1] == "--plot":
        utils.plot(
            preference.func,
            preference.r,
            blocking=False,
            x_lim=preference.XLIM,
            y_lim=preference.YLIM,
        )
    result: float = (
        GradientDescent()
        .apply(
            initial_param=preference.INIT_PARAM,
            learning_rate=0.02,
            num_iterations=preference.NUM_ITER,
            early_stop_policy=utils.template_early_stop,
            func_differential=lambda x: preference.func_difference.subs(
                preference.r, x
            ),  # type: ignore
        )
        .invoke()
    )
    exec(utils.method_name)
    utils.stat(result)

    # Expected: 3.8219832744 × 10⁻¹⁰


if __name__ == "__main__":
    try:
        run()
    except KeyboardInterrupt:
        pass
